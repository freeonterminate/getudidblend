﻿(*
 * GetUDID Project
 *
 * PLATFORMS
 *   Windows
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * 2018/08/30 Version 1.0.0
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

program GetUDID;

uses
  System.StartUpCopy,
  FMX.Forms,
  uMain in 'uMain.pas' {frmMain},
  PK.HardInfo.iPhoneInfo.Win in 'Libs\PK.HardInfo.iPhoneInfo.Win.pas',
  PK.HardInfo.WMI.Win in 'Libs\PK.HardInfo.WMI.Win.pas',
  PK.Utils.Clipboard in 'Libs\PK.Utils.Clipboard.pas',
  PK.Utils.Registry in 'Libs\PK.Utils.Registry.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
