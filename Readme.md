# GetUDID

## Overview
This application gets UDID of iOS device on Windows.  

## ScreenShot
Screenshots use the style of DelphiStyle.com.  
Blend style is used in this source.  
![ScreenShot](https://bitbucket.org/freeonterminate/getudidblend/raw/b58e45f3e4d25afcdb9aaf66d98bc8373fd4f5ec/Images/screenshot.png)


## Environment
Delphi 10.2 Tokyo Release 3  
FireMonkey  

## Support OS
Windows  

## How to use
0. Connect iPhone to Windows  
1. Start GetUDID.  
2. Press the [Get] button  
3. The UDID of the connected iPhone will be displayed  
4. If more than one iPhone is connected, you can select from the dropdown list  
5. Click on UDID to copy to clipboard  

## Contact
freeonterminate@gmail.com  
http://twitter.com/pik  

# LICENSE
Copyright (c) 2018 HOSOKAWA Jun  
Released under the MIT license  
http://opensource.org/licenses/mit-license.php
