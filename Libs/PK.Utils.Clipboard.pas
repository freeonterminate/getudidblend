﻿(*
 * Clipboard Utility
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * 2018/08/30 Version 1.0.0
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit PK.Utils.Clipboard;

interface

uses
  System.SysUtils, System.Rtti, FMX.Platform;

type
  TClipboard = record
  private type
    TClipboardHandler =
      reference to procedure(const iService: IFMXClipboardService);
  private
    class function GeTClipboard(
      const iProc: TClipboardHandler): Boolean; static;
  public
    class function Copy(iValue: TValue): Boolean; static;
    class function GetAsString: String; static;
  end;


implementation

{ TClipboard }

class function TClipboard.Copy(iValue: TValue): Boolean;
begin
  Result :=
    GeTClipboard(
      procedure(const iService: IFMXClipboardService)
      begin
        if iService <> nil then
          iService.SetClipboard(iValue);
      end
    );
end;

class function TClipboard.GetAsString: String;
var
  Res: String;
begin
  Res := '';

  GeTClipboard(
    procedure(const iService: IFMXClipboardService)
    begin
      if iService <> nil then
      begin
        Res := iService.GetClipboard.AsString;
      end;
    end
  );

  Result := Res;
end;

class function TClipboard.GeTClipboard(
  const iProc: TClipboardHandler): Boolean;
var
  Service: IFMXClipboardService;
begin
  Result :=
    TPlatformServices.Current.SupportsPlatformService(
      IFMXClipboardService,
      Service
    );

  if not Result then
     Service := nil;

  if Assigned(iProc) then
    iProc(Service);
end;

end.
