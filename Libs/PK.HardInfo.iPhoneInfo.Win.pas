﻿(*
 * iPhone Info (UDID)
 *
 * PLATFORMS
 *   Windows
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * 2018/08/31 Version 1.0.0
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

 unit PK.HardInfo.iPhoneInfo.Win;

{$IFNDEF MSWINDOWS}
{$WARNINGS OFF 1011}
interface
implementation
end.
{$ENDIF}

interface

type
  TPhoneInfo = record
  public const
    CATEGORY = 'USB';
    CAPTION = 'Caption';
    VENDER_ID = 'VID_05AC'; // Apple
    PRODUCT_ID_HEADER = 'PID_';
    UUID_LEN = 40;
    ENUM_ADDRESS = 'SYSTEM\CurrentControlSet\Enum\USB\VID_05AC&PID_%s';
  private
    FUDID: String;
    FPID: String;
    FModel: String;
  public
    class function Create(const iDeviceIDValue: String): TPhoneInfo; static;
    procedure Clear;
    property UDID: String read FUDID;
    property PID: String read FPID;
    property Model: String read FModel;
  end;

implementation

uses
  System.SysUtils
  , Winapi.Windows
  , PK.Utils.Registry
  ;

const
  OTHER = 'Unknown (%s)';
  PHONES: array [0.. 54] of TPhoneInfo = (
    (FPID: '1201'; FModel: '3G iPod'),
    (FPID: '1202'; FModel: 'iPod 2G'),
    (FPID: '1203'; FModel: 'iPod 4.Gen Grayscale 40G'),
    (FPID: '1204'; FModel: 'iPod [Photo]'),
    (FPID: '1205'; FModel: 'iPod Mini 1.Gen/2.Gen'),
    (FPID: '1206'; FModel: 'iPod 06'),
    (FPID: '1207'; FModel: 'iPod 07'),
    (FPID: '1208'; FModel: 'iPod 08'),
    (FPID: '1209'; FModel: 'iPod Video'),
    (FPID: '120A'; FModel: 'iPod Nano'),

    (FPID: '1223'; FModel: 'iPod Classic/Nano 3.Gen (DFU mode)'),
    (FPID: '1224'; FModel: 'iPod Nano 3.Gen (DFU mode)'),
    (FPID: '1225'; FModel: 'iPod Nano 4.Gen (DFU mode)'),
    (FPID: '1227'; FModel: 'Mobile Device (DFU Mode)'),

    (FPID: '1231'; FModel: 'iPod Nano 5.Gen (DFU mode)'),

    (FPID: '1240'; FModel: 'iPod Nano 2.Gen (DFU mode)'),
    (FPID: '1242'; FModel: 'iPod Nano 3.Gen (WTF mode)'),
    (FPID: '1243'; FModel: 'iPod Nano 4.Gen (WTF mode)'),
    (FPID: '1245'; FModel: 'iPod Classic 3.Gen (WTF mode)'),
    (FPID: '1246'; FModel: 'iPod Nano 5.Gen (WTF mode)'),

    (FPID: '1255'; FModel: 'iPod Nano 4.Gen (DFU mode)'),

    (FPID: '1260'; FModel: 'iPod Nano 2.Gen'),
    (FPID: '1261'; FModel: 'iPod Classic'),
    (FPID: '1262'; FModel: 'iPod Nano 3.Gen'),
    (FPID: '1263'; FModel: 'iPod Nano 4.Gen'),
    (FPID: '1265'; FModel: 'iPod Nano 5.Gen'),
    (FPID: '1266'; FModel: 'iPod Nano 6.Gen'),
    (FPID: '1267'; FModel: 'iPod Nano 7.Gen'),

    (FPID: '1281'; FModel: 'Apple Mobile Device [Recovery Mode]'),

    (FPID: '1290'; FModel: 'iPhone'),
    (FPID: '1291'; FModel: 'iPod Touch 1.Gen'),
    (FPID: '1292'; FModel: 'iPhone 3G'),
    (FPID: '1293'; FModel: 'iPod Touch 2.Gen'),
    (FPID: '1294'; FModel: 'iPhone 3GS'),
    (FPID: '1296'; FModel: 'iPod Touch 3.Gen (8GB)'),
    (FPID: '1297'; FModel: 'iPhone 4'),
    (FPID: '1299'; FModel: 'iPod Touch 3.Gen'),
    (FPID: '129A'; FModel: 'iPad'),
    (FPID: '129C'; FModel: 'iPhone 4(CDMA)'),
    (FPID: '129E'; FModel: 'iPod Touch 4.Gen'),
    (FPID: '129F'; FModel: 'iPad 2'),

    (FPID: '12A0'; FModel: 'iPhone 4S'),
    (FPID: '12A2'; FModel: 'iPad 2 (3G; 64GB)'),
    (FPID: '12A3'; FModel: 'iPad 2 (CDMA)'),
    (FPID: '12A4'; FModel: 'iPad 3 (wifi)'),
    (FPID: '12A5'; FModel: 'iPad 3 (CDMA)'),
    (FPID: '12A6'; FModel: 'iPad 3 (3G, 16 GB)'),
    (FPID: '12A8'; FModel: 'iPhone 5 or later'),
    (FPID: '12A9'; FModel: 'iPad 2'),
    (FPID: '12AA'; FModel: 'iPod Touch 5.Gen [A1421]'),
    (FPID: '12AB'; FModel: 'iPad 4/Mini1'),

    (FPID: '1300'; FModel: 'iPod Shuffle'),
    (FPID: '1301'; FModel: 'iPod Shuffle 2.Gen'),
    (FPID: '1302'; FModel: 'iPod Shuffle 3.Gen'),
    (FPID: '1303'; FModel: 'iPod Shuffle 4.Gen')
  );

{ TfrmMain.TPhoneInfo }

procedure TPhoneInfo.Clear;
begin
  FUDID := '';
  FPID := '';
  FModel := '';
end;

class function TPhoneInfo.Create(
  const iDeviceIDValue: String): TPhoneInfo;
var
  Props: TArray<String>;
  PID: String;
  UDID: String;
  Info: TPhoneInfo;
  Keys: TArray<String>;
  Key: String;
begin
  Result.Clear;

  if (not iDeviceIDValue.StartsWith(CATEGORY)) then
    Exit;

  Props := iDeviceIDValue.Split(['\', '&']);

  if Length(Props) < 4 then
    Exit;

  if not Props[1].StartsWith(VENDER_ID) then
    Exit;

  if not Props[2].StartsWith(PRODUCT_ID_HEADER) then
    Exit;

  PID := Props[2].Substring(PRODUCT_ID_HEADER.Length);
  UDID := Props[3];

  if UDID.Length <> UUID_LEN then
  begin
    Keys :=
      TRegistry.GetSubKeys(HKEY_LOCAL_MACHINE, Format(ENUM_ADDRESS, [PID]));

    for Key in Keys do
    begin
      if Key.Length = UUID_LEN then
      begin
        UDID := Key.ToUpper;
        Break;
      end;
    end;
  end;

  if UDID.Length <> UUID_LEN then
    Exit;

  Result.FUDID := UDID;
  Result.FPID := PID;
  Result.FModel := Format(OTHER, [PID]);

  for Info in PHONES do
    if Result.FPID = Info.FPID then
    begin
      Result.FModel := Info.FModel;
      Break;
    end;
end;

end.
