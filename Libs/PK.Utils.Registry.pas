﻿(*
 * Registry Utils
 *
 * PLATFORMS
 *   Windows
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * 2004/07/11 Version 0.0.0
 * 2018/05/19 Version 1.0.0  Ported to Delphi 10.2 Tokyo
 * 2018/09/09 Version 1.1.0  Change to Advanced record
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

 unit PK.Utils.Registry;

interface

{$IFNDEF MSWINDOWS}
{$WARNINGS OFF 1011}
interface
implementation
end.
{$ENDIF}

uses
  Winapi.Windows;

type
  TRegistry = record
  private
    class function DeleteDelimiter(const iStr: String): String; static;
    class function OpenKey(
      const iRegKey: HKEY;
      const iAddress: String;
      const iAccess: DWORD;
      out oReg: HKEY): Boolean; static;
  public
    class function DeleteReg(
      const iRegKey: HKEY;
      const iAddress: String): Boolean; static;
    class function ExistsReg(
      const iRegKey: HKEY;
      const iAddress: String): Boolean; static;
    class function GetReg(
      const iRegKey: HKEY;
      const iAddress, iKey: String): String; static;
    class function GetSubKeys(
      const iRegKey: HKEY;
      const iAddress: String): TArray<String>; static;
    class function SetReg(
      const iRegKey: HKEY;
      const iAddress, iKey, iValue: String): Boolean; static;
  end;

implementation

uses
  System.SysUtils
  , Winapi.Messages
  ;

class function TRegistry.DeleteDelimiter(const iStr: String): String;
begin
  if Result.EndsWith('\') then
    Result := Copy(Result, 1, Length(Result) - 1)
  else
    Result := iStr;
end;

class function TRegistry.DeleteReg(
  const iRegKey: HKEY;
  const iAddress: String): Boolean;
var
  Len: Integer;
  Name: String;
  NameLen: Cardinal;
  FT: TFileTime;
  tmpAddr: String;

  function DeleteRegSub(iAddress: String): Boolean;
  var
    i: Integer;
    RetVal: DWORD;
    hReg: HKEY;
  begin
    Result := False;

    Len := Length(iAddress);

    if (Len < 1) then
      Exit;

    if (not iAddress.EndsWith('\')) then
      iAddress := iAddress + '\';

    if OpenKey(iRegKey, iAddress, KEY_ALL_ACCESS, hReg) then
      try
        i := 0;

        repeat
          SetLength(Name, MAX_PATH);
          NameLen := Length(Name);

          RetVal :=
            RegEnumKeyEx(hReg, i, PChar(Name), NameLen, nil, nil, nil, @FT);

          if (RetVal = ERROR_SUCCESS) then
          begin
            SetLength(Name, NameLen);

            if (DeleteRegSub(iAddress + Name)) then
              Dec(i);
          end
          else
          begin
            tmpAddr := iAddress;

            if
              (Win32Platform <> VER_PLATFORM_WIN32_NT) and
              (tmpAddr[Len] = '\')
            then
              tmpAddr := DeleteDelimiter(tmpAddr);

            Result :=
              (RegDeleteKey(iRegKey, PChar(tmpAddr)) = ERROR_SUCCESS);
          end;

          Inc(i);
        until (RetVal <> ERROR_SUCCESS);
      finally
        RegCloseKey(hReg);
      end;
  end;

begin
  DeleteRegSub(iAddress);
  Result := not ExistsReg(iRegKey, iAddress);
end;

class function TRegistry.ExistsReg(
  const iRegKey: HKEY;
  const iAddress: String): Boolean;
var
  hReg: HKEY;
begin
  try
    Result := OpenKey(iRegKey, iAddress, KEY_ALL_ACCESS, hReg);
  finally
    RegCloseKey(hReg);
  end;
end;

class function TRegistry.GetReg(
  const iRegKey: HKEY;
  const iAddress, iKey: String): String;
var
  hReg: HKEY;
  RegType: DWORD;
  VarSize: DWORD;
  RetVal: DWORD;
begin
  hReg := 0;
  RegType := REG_SZ;
  SetLength(Result, $ff);
  VarSize := Length(Result);

  if OpenKey(iRegKey, iAddress, KEY_ALL_ACCESS, hReg) then
    try
      RetVal :=
        RegQueryValueEx(
          hReg,
          PChar(iKey),
          nil,
          @RegType,
          PByte(PChar(Result)),
          @VarSize
        );

      if RetVal = ERROR_SUCCESS then
      begin
        SetLength(Result, VarSize);
        Result := Result.Trim;
      end
      else
        Result := '';
    finally
      RegCloseKey(hReg);
    end;
end;

class function TRegistry.GetSubKeys(
  const iRegKey: HKEY;
  const iAddress: String): TArray<String>;
var
  hReg: HKEY;
  Buff: String;
  BuffLen: DWORD;
  LastWriteTime: TFileTime;
  i: Integer;
  Count: Integer;
  RetVal: DWORD;
begin
  Count := 0;
  SetLength(Result, 0);

  if OpenKey(iRegKey, iAddress, KEY_READ, hReg) then
    try
      SetLength(Buff, MAX_PATH);
      BuffLen := Buff.Length;

      for i := 0 to $ffff do
      begin
        RetVal :=
          RegEnumKeyEx(
            hReg,
            i,
            PChar(Buff),
            BuffLen,
            nil,
            nil,
            nil,
            @LastWriteTime);

        case RetVal of
          ERROR_SUCCESS:
          begin
            SetLength(Result, Count + 1);
            Result[Count] := Buff.Substring(0, BuffLen);
            Inc(Count);
          end;

          ERROR_NO_MORE_ITEMS:
          begin
            Break;
          end;
        end;
      end;
    finally
      RegCloseKey(hReg);
    end;
end;

class function TRegistry.OpenKey(
  const iRegKey: HKEY;
  const iAddress: String;
  const iAccess: DWORD;
  out oReg: HKEY): Boolean;
begin
  oReg := 0;

  Result :=
    RegOpenKeyEx(
      iRegKey,
      PChar(DeleteDelimiter(iAddress)),
      0,
      iAccess,
      oReg)
    = ERROR_SUCCESS;
end;

class function TRegistry.SetReg(
  const iRegKey: HKEY;
  const iAddress, iKey, iValue: String): Boolean;
var
  hReg: HKEY;
  Disposition: DWORD;
  Res: LONG;
begin
  Result := False;

  hReg := 0;

  Res :=
    RegCreateKeyEx(
      iRegKey,
      PChar(iAddress),
      0,
      nil,
      REG_OPTION_NON_VOLATILE,
      KEY_ALL_ACCESS,
      nil,
      hReg,
      @Disposition);
  try
    if Res <> ERROR_SUCCESS then
      Exit;

    Res :=
      RegSetValueEx(
        hReg,
        PChar(iKey),
        0,
        REG_SZ,
        PChar(iValue),
        Length(iValue) * SizeOf(Char));

    Result := Res = ERROR_SUCCESS;
  finally
    RegCloseKey(hReg);
  end;
end;

end.
