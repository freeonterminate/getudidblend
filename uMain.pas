﻿(*
 * GetUDID Main
 *
 * PLATFORMS
 *   Windows
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * 2018/08/31 Version 1.0.0
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit uMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.Controls.Presentation, FMX.Edit, FMX.ListBox, FMX.Objects,
  FMX.Effects, FMX.Ani, PK.HardInfo.iPhoneInfo.Win;

type
  TfrmMain = class(TForm)
    btnGet: TButton;
    layoutRoot: TLayout;
    layoutTop: TLayout;
    layoutBottom: TLayout;
    layoutClient: TLayout;
    edtUDID: TEdit;
    layoutCenter: TLayout;
    cmbbxProductIDs: TComboBox;
    layoutCopyUDID: TLayout;
    rectDisabler: TRectangle;
    textDisabler: TText;
    rectCopied: TRoundRect;
    animOpacity: TFloatAnimation;
    effectCopied: TGlowEffect;
    textCopied: TText;
    lblTitle: TLabel;
    styleBook: TStyleBook;
    procedure btnGetClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure layoutCopyUDIDClick(Sender: TObject);
    procedure cmbbxProductIDsChange(Sender: TObject);
    procedure animOpacityFinish(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  private const
    NONE = '(none)';
    ERROR_NONE = '(error: device not found)';
    ERROR_WMI = '(error: wmi)';
  private var
    FPhoneInfos: TArray<TPhoneInfo>;
  private
    procedure SetNone;
  public
  end;

var
  frmMain: TfrmMain;

implementation

uses
  PK.Utils.Clipboard
  , PK.HardInfo.WMI.Win
  ;

{$R *.fmx}

{ TfrmMain }

procedure TfrmMain.animOpacityFinish(Sender: TObject);
begin
  rectCopied.Visible := False;
end;

procedure TfrmMain.btnGetClick(Sender: TObject);
const
  CLASS_NAME = 'Win32_PnPEntity';
  DEVICE_ID = 'DeviceID';
begin
  SetNone;

  rectDisabler.Visible := True;
  try
    Application.ProcessMessages;

    if
      TWMI.GetProperty(
        CLASS_NAME,
        [DEVICE_ID],
        procedure(const iProps: TWMI.TWbemPropDic)
        var
          DeviceIDVariant: Variant;
          Len: Integer;
          Info: TPhoneInfo;
        begin
          if not iProps.TryGetValue(DEVICE_ID, DeviceIDVariant) then
            Exit;

          Info := TPhoneInfo.Create(DeviceIDVariant);
          if Info.UDID.IsEmpty then
            Exit;

          Len := Length(FPhoneInfos);
          SetLength(FPhoneInfos, Len + 1);
          FPhoneInfos[Len] := Info;

          cmbbxProductIDs.Items.Add(Info.Model);
        end
      )
    then
    begin
      if cmbbxProductIDs.Items.Count > 0 then
      begin
        cmbbxProductIDs.ItemIndex := 0;
        cmbbxProductIDsChange(Self);
      end
      else
        edtUDID.Text := ERROR_NONE;
    end
    else
    begin
      edtUDID.Text := ERROR_WMI;
    end;
  finally
    rectDisabler.Visible := False;
  end;
end;

procedure TfrmMain.cmbbxProductIDsChange(Sender: TObject);
var
  Index: Integer;
begin
  Index := cmbbxProductIDs.ItemIndex;

  if (Index > -1) and (Index < Length(FPhoneInfos)) then
  begin
    edtUDID.Text := FPhoneInfos[Index].UDID;
    edtUDID.SelLength := 0;
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  SetNone;

  rectCopied.Visible := False;

  rectDisabler.Visible := False;
  rectDisabler.Align := TAlignLayout.Contents;
  rectDisabler.BringToFront;
end;

procedure TfrmMain.FormKeyUp(
  Sender: TObject;
  var Key: Word;
  var KeyChar: Char;
  Shift: TShiftState);
begin
  if Key = vkEscape then
    Close;
end;

procedure TfrmMain.layoutCopyUDIDClick(Sender: TObject);
begin
  if edtUDID.Text.StartsWith('(') then
    Exit;

  TClipboard.Copy(edtUDID.Text);
  rectCopied.Visible := True;
  animOpacity.Start;
end;

procedure TfrmMain.SetNone;
begin
  edtUDID.Text := NONE;
  cmbbxProductIDs.Items.Clear;
  SetLength(FPhoneInfos, 0);
end;

end.
